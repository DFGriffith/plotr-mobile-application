//------ MONGO COLLECTIONS ------//

Posts = new Mongo.Collection("posts");

Likes = new Mongo.Collection("likes");

Comments = new Mongo.Collection("comments");

Profiles = new Mongo.Collection("profiles");

UserImages = new Mongo.Collection("userImages");

Connections = new Mongo.Collection("connections");



//------ GLOBAL DATA ------//

var postLocations = [];
var postTitles = [];
var postIds = [];
var connectionOwners = [];
//var connectionIds = [];



//------ ROUTES ------//

Router.route('/', {
    name: 'home',
    template: 'home'
});

Router.route('/map');

Router.route('/connections/:_id', {
    template: 'connections',
    name: 'connections',
    data: function(){
        var params = this.params; // { _id: "5" }
        var userId = params._id; // "5"

        return Meteor.users.findOne({ _id: userId });
    }
});

Router.route('/profile/:_id', {
    template: 'profile',
    name: 'profile',
    data: function(){
        var params = this.params; // { _id: "5" }
        var userId = params._id; // "5"

        return Profiles.findOne({ owner: userId });
    }
});

Router.route('/post/:_id', {
    template: 'singlePost',
    name: 'singlePost',
    data: function(){
        var params = this.params; // { _id: "5" }
        var postId = params._id; // "5"

        return Posts.findOne({ _id: postId });
    }
});

Router.configure({
    layoutTemplate: 'main'
});





//if (Meteor.isCordova) {
//  console.log("Printed only in mobile cordova apps");
//}

//------ This code only runs on the server ------//

if (Meteor.isServer) {
  // Only publish tasks that are public or belong to the current user
  Meteor.publish("posts", function () {
    var conns = Connections.find({owner: this.userId});
    var connIds = [];
      
    conns.forEach(getUserIds);
    
    for (con in connectionOwners){
        connIds.push(connectionOwners[con]);    
    }
      
    connectionOwners = [];
      
    return Posts.find({
      $or: [
        { private: {$ne: true} },
        { owner: this.userId },
        { owner: {$in: connIds} }
      ]      
    });
  });
    
  Meteor.publish("likes", function () {
    return Likes.find();
  });
    
  Meteor.publish("comments", function () {
    return Comments.find();
  });
  
  Meteor.publish("profiles", function () {
    return Profiles.find();
  });
  
  Meteor.publish("userImages", function () {
    return UserImages.find();
  });
  
  Meteor.publish("connections", function () {
    return Connections.find();
  });
    
  Meteor.publish("allUsers", function () {
    return Meteor.users.find({});
  });
    
    
  // ----- Slingshot ----- //
  
  Slingshot.fileRestrictions( "uploadToAmazonS3", {
    allowedFileTypes: [ "image/png", "image/jpeg", "image/gif" ],
    maxSize: 25 * 1024 * 1024
  });

  Slingshot.createDirective( "uploadToAmazonS3", Slingshot.S3Storage, {
    bucket: "here-after-images",
    acl: "public-read",
    region: "ap-southeast-2",
    authorize: function () {
      let userFileCount = UserImages.find( { "userId": this.userId } ).count();
      return userFileCount < 3 ? true : false;
    },
    key: function ( file ) {
      var user = Meteor.users.findOne( this.userId );
      return user.emails[0].address + "/" + file.name;
    }
  });
    
//  Slingshot.fileRestrictions( "uploadUserImageToAmazonS3", {
//    allowedFileTypes: [ "image/png", "image/jpeg", "image/gif" ],
//    maxSize: 25 * 1024 * 1024
//  });
//
//  Slingshot.createDirective( "uploadUserImageToAmazonS3", Slingshot.S3Storage, {
//    bucket: "here-after-images",
//    acl: "public-read",
//    region: "ap-southeast-2",
//    authorize: function () {
//      let userFileCount = UserImages.find( { "userId": this.userId } ).count();
//      return userFileCount < 3 ? true : false;
//    },
//    key: function ( file ) {
//      var user = Meteor.users.findOne( this.userId );
//      return user.emails[0].address + "/" + file.name;
//    }
//  });
    
  Slingshot.fileRestrictions( "uploadVidToAmazonS3", {
    allowedFileTypes: [ "video/webm", "video/mp4", "video/mov", "video/quicktime", "video/3gp"],
    maxSize: 50 * 1024 * 1024
  });

  Slingshot.createDirective( "uploadVidToAmazonS3", Slingshot.S3Storage, {
    bucket: "here-after-videos",
    acl: "public-read",
    region: "ap-southeast-2",

    authorize: function () {
      var user = Meteor.userId();
      if(user){
        return true;
      } else {
        return false;
      }
    },

    key: function ( file ) {
      var user = Meteor.users.findOne( this.userId );
      return user.emails[0].address + "/" + file.name;
    }
  });    
    

  // ----- Accounts ----- //
    
  Accounts.onCreateUser(function(options, user) {
      var bioText = "Hi my name is " + user.username;
      
      console.log(user._id);
      
      Profiles.insert({
        bio: bioText,
        createdAt: new Date(),
        owner: user._id,
        username: user.username,
        image: null
      });
      
      return user;
  });
}




//------ This code only runs on the client ------//
 
if (Meteor.isClient) { 
  var MAP_ZOOM = 12.5;
    
  Meteor.subscribe("posts");
  Meteor.subscribe("likes");
  Meteor.subscribe("comments");
  Meteor.subscribe("profiles");
  Meteor.subscribe("connections");
  Meteor.subscribe('allUsers');
  Meteor.subscribe('userImages');
    
    
  // ----- Main ----- //
  Template.main.helpers({
    userId: function () {
         return Meteor.userId();
    }
  });
    
  Template.navigation.helpers({
    userId: function () {
         return Meteor.userId();
    }
  }); 
    
    
    
    
  Template.videoCaptureBasic.helpers({
    opts: function() {
      var opts ={
          maxTime: 10,
        // androidQuality: 0,
//         videoDisplay: {
//           width: 600,
//           height: 460
//         },
         classes: {
           recordBtn: 'btn btn-info',
         },
        onVideoRecorded: function(err, base64Data) {
//            console.log('onVideoRecorded: ', base64Data);
            document.getElementById("file_vid").value = base64Data;
        }
      };
      return opts;
    }
  });
    
    
  Template.imageCaptureMdg.events({
    'click .dd-button': function () {
      event.preventDefault();
      var cameraOptions = {
        width: 800,
        height: 600
      };

      MeteorCamera.getPicture(cameraOptions, function (error, data) {
          if(error){
            alert(error.error);
          }else{
            document.getElementById("file_vid").value = data;
          }
      });
    }
  });
    
    
    
    
    
    
    
  // ----- Posts ----- //
    
  Template.posts.helpers({
    posts: function () {
        var indexs = [];
        
        var posts = Posts.find({}, {sort: {createdAt: -1}});
        posts.forEach(getPostLocations);
        
        var userloc = Geolocation.latLng();
        if (! userloc)
          return;
        
        for (post in postLocations){  
            if (! postLocations[post])
                continue;     

            var d = calcDistance(postLocations[post], userloc);
            
            if (d <= 100 && d >= 0){
                indexs.push(postLocations[post]);
            }
        }
       
        var inRangePosts = Posts.find( { geo: { $in: indexs} }, {sort: {createdAt: -1}} );
        var count = Posts.find().count();
        postLocations = [];
        
        console.log("posts : ", count);
        
        return inRangePosts;
    },
    postCountTrue: function () {
        var indexs = [];
        
        var posts = Posts.find({}, {sort: {createdAt: -1}});
        posts.forEach(getPostLocations);
        
        var userloc = Geolocation.latLng();
        if (! userloc)
          return;
        
        for (post in postLocations){  
            if (! postLocations[post])
                continue;     

            var d = calcDistance(postLocations[post], userloc);
            
            if (d <= 100 && d >= 0){
                indexs.push(postLocations[post]);
            }
        }
       
        var inRangePostCount = Posts.find( { geo: { $in: indexs} }, {sort: {createdAt: -1}} ).count();
        postLocations = [];
        
        return inRangePostCount > 0 ? true : false;
    },
    incompleteCount: function () {
      return Tasks.find().count();
    }
  });
    
//  storeUrlInPostDatabase: function( url, location, title ) {
//    check( url, String );
//    validate( url );
//      
//    try {
//      Posts.insert({
//        title: title,
//        url: url,
//        geo: location,
//        createdAt: new Date(),
//        owner: Meteor.userId(),
//        username: Meteor.user().username
//      });
//    } catch( exception ) {
//        console.log("store post exception");
//        return exception;
//    }
//  }
    
  Template.addPost.events({
    "submit .new-task": function (event, template) {
      // Prevent default browser form submit
      event.preventDefault();
 
      // Get value from form element
      var text = event.target.text.value;
        
      var fileb64data = event.target.file.value;
//      var filename = createFilename();
//        
//      var x = addfilenameType(filename, fileb64data);
//        
//      var file = dataUriToFile(fileb64data, x);
//      var file = files.files[0];
//       
//        console.log("filename : ", x);
         console.log("file : ", fileb64data);
        
      var location = Geolocation.latLng();
        
      if(! location){
        bert.alert("Location not available, please check your settings", "warning"); 
        return;
      }
      if(! fileb64data){
        return;
      }
        
        
      Meteor.call("storeUrlInPostDatabase", fileb64data, location, text)
        
//      if(isImg(file.name)){
//        upload ( { event: event, template: template, type :"image", location: location, title: text, file: file } );
//      }
//      else if(isVid(file.name)){
//        upload ( { event: event, template: template, type :"video", location: location, title: text, file: file } );  
//      }
//      else{
//        bert.alert( "File format not accepted", "warning" );
//      }
 
      // Clear form
      event.target.text.value = "";
      event.target.file.value = "";
    },
    'click .dd-photo-button': function (event, template) {
        console.log("");
        console.log("click photo button..........");
        console.log("");
      event.preventDefault();
      var cameraOptions = {
        width: 600,
        height: 600
      };

      MeteorCamera.getPicture({}, function (error, data) {
          if(error){
            alert(error.error);
          }else{
            document.getElementById("file_vid").value = data;
          }
      });
    }
//    "onclick .dropbtn": function (event, template) {
//        document.getElementById("myDropdown").classList.toggle("show");
//    },
//    "onclick window": function (event, template) {
//        if (!event.target.matches('.dropbtn')) {
//          var dropdowns = document.getElementsByClassName("dropdown-content");
//          var i;
//          for (i = 0; i < dropdowns.length; i++) {
//            var openDropdown = dropdowns[i];
//            if (openDropdown.classList.contains('show')) {
//                openDropdown.classList.remove('show');
//            }
//          }
//        } 
//    }
//    'change input[type="file"]': function ( event, template ) {
      
//    }
  });
    

  Template.post.helpers({
    isOwner: function () {
      return this.owner === Meteor.userId();
    },
    userHasImage: function (ownerId) {
        var imageCount = UserImages.find({userId: ownerId}).count();
        return imageCount > 0 ? true : false;
    },
    userImageUrl: function (ownerId){
        var image = UserImages.findOne({userId: ownerId});
        return image.url;
    },
    postIsImage: function (url){
      const formats = [ 'jpg', 'jpeg', 'png', 'gif' ];
        
        var blah = isImg2(url);
        console.log("blah : ", blah);
        
      return blah;//_.find( formats, ( format ) => url.indexOf( format ) > -1 );
    },
    postLikeCount: function(postId){
      var postCount = Likes.find({post: postId}).count();
      return postCount;
    },
    postCommentCount: function(postId){
      var commentCount = Comments.find({post: postId}).count();
      return commentCount;
    },
    liked: function () {
        var like = Likes.find({owner: Meteor.userId()}).count();
        return like > 0 ? true : false;
    }
  });
    
  Template.post.events({
    "click .delete": function () {
      Meteor.call("deletePost", this._id);
    },
    "click .toggle-private": function () {
      Meteor.call("setPostPrivate", this._id, ! this.private);
    },
    "click .toggle-like": function () {
      var isliked = Likes.find({owner: Meteor.userId()}).count();
        
      if (isliked > 0)
        Meteor.call("unlikePost", this._id);
      else 
        Meteor.call("likePost", this._id);
    },
    'click .js-open-popup': function (e) {
        var imgPath = $(e.currentTarget).data('image');
        if (imgPath) {
            sImageBox.open(imgPath, {
                animation: 'zoomIn'
            });
        }
    }
  });
    
    
  Template.singlePost.events({
    "submit .new-task": function (event, template) {
      // Prevent default browser form submit
      event.preventDefault();
 
      // Get value from form element
      var text = event.target.text.value;
        
      Meteor.call("commentPost", text, this._id); 
      // Clear form
      event.target.text.value = "";
    }
  });
    
  Template.singlePost.helpers({
    comments: function (postId){
        var comments = Comments.find({post: postId});
        return comments;
    }
  });
    
  Template.comment.helpers({
    isOwner: function () {
      return this.owner === Meteor.userId();
    },
    userHasImage: function (ownerId) {
        var imageCount = UserImages.find({userId: ownerId}).count();
        return imageCount > 0 ? true : false;
    },
    userImageUrl: function (ownerId){
        var image = UserImages.findOne({userId: ownerId});
        return image.url;
    },
    username: function (ownerId){
        var user = Meteor.users.findOne({_id: ownerId});
        return user.username;
    },
    postLikeCount: function(postId){
      var postCount = Likes.find({post: postId}).count();
      return postCount;
    },
    liked: function () {
        var like = Likes.find({owner: Meteor.userId()}).count();
        return like > 0 ? true : false;
    }
    
  });

    

    

    
  // ----- Profile ----- //    
    
  Template.profile.events({
      "submit .add-connection": function (event) {
          var connection = event.target.ownerId.value;
          
          Meteor.call("addConnection", connection);
      },
      "submit .remove-connection": function (event) {
          var connection = event.target.ownerId.value;
          
          Meteor.call("removeConnection", connection);
      },
      'click .js-open-popup': function (e) {
        var imgPath = $(e.currentTarget).data('image');
        if (imgPath) {
            sImageBox.open(imgPath, {
                animation: 'zoomIn'
            });
        }
      }
  }); 

  Template.profile.helpers({
    hasImage: function (ownerId) {
        var imageCount = UserImages.find({userId: ownerId}).count();
        return imageCount > 0 ? true : false;
    },
    imageUrl: function (ownerId){
        var image = UserImages.findOne({userId: ownerId});
        return image.url;
    },
    isConnected: function (conId) {
        var conn = Connections.find( { $and: [ 
            {owner: Meteor.userId() }, 
            {connection_id: this.owner }
        ]}).count();
        
        return conn > 0 ? true : false;
    },
    isOwner: function () {
      return this.owner === Meteor.userId();
    },
    usersPosts: function (userId) {
        var posts = Posts.find({owner: userId}, {sort: {createdAt: -1}});
        return posts;
    }
  });
    
  Template.upload_userImage.events({
    'change input[type="file"]' ( event, template ) {
      var file = event.target.files[0];
        console.log(file);
      upload ( { event: event, template: template, type :"userImage", file: file } );
    }
  });
    
   Template.editBio.events({
    'click #add': function(e) {
      e.preventDefault();
      $('#bioModal').modal('show');
    }
  });   
    
  Template.modalTemplate.events({
    'click #save': function(e) {
      e.preventDefault();

      var bioText = {
        bio: $('#bioText').val()
      }

      Meteor.call('updateBio', bioText, function(error, result) {
        if (error) {
          alert(error);
        }
      });

      $('#bioModal').modal('hide');
    }
  });
    

    
  // ----- Files ----- //    
    
  Template.files.onCreated( () => Template.instance().subscribe( 'files' ) );

  Template.files.helpers({
    files() {
      var files = UserImages.find( {}, { sort: { "added": -1 } } );
      if ( files ) {
        return files;
      }
    }
  });
    
  Template.file.helpers({
    isImage( url ) {
      const formats = [ 'jpg', 'jpeg', 'png', 'gif' ];
      return _.find( formats, ( format ) => url.indexOf( format ) > -1 );
    }
  });
    
    
    
  // ----- Users ----- //
    
  Template.users.helpers({
    cons: function (ownerId) {
        var ids = [];
        
        var conns = Connections.find({owner: ownerId}, {sort: {createdAt: -1}});
        conns.forEach(getUserIds);
        
        for (con in connectionOwners){  
            if (! connectionOwners[con])
                continue;     
            ids.push(connectionOwners[con]);
        }
        
        var users = Meteor.users.find( { "_id": { $in: ids} }, {sort: {createdAt: -1}} );
        connectionOwners = [];
        return users;
    }
  });
  
  Template.user.helpers({
    hasImage: function (ownerId) {
        var imageCount = UserImages.find({userId: ownerId}).count();
        return imageCount > 0 ? true : false;
    },
    imageUrl: function (ownerId){
        var image = UserImages.findOne({userId: ownerId});
        return image.url;
    },
    joined: function (date){
        return date.toLocaleDateString();
    }
  });
    
  Accounts.ui.config({
    passwordSignupFields: "USERNAME_AND_EMAIL"
  });
    
    
    
 
    
    
  // ----- Map ----- //
  Template.map.onRendered(function() {
    GoogleMaps.load();
  });
    
  Template.map.onCreated(function() {
    var self = this;

    GoogleMaps.ready('map', function(map) {
      var marker;
      var postMarkers = [];
      var postMarkerContent = [];
        
        var infowindow = null;
        var contentString = null;
        
        var image = {
            url: 'images/map-post-photo-icon-cyan-ps.png',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(25, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(12, 32)
        };

    
        
        if(! postLocations[0]){
            var posts = Posts.find();
            posts.forEach(getPostLocations);
            
            for (post in postLocations){ 
                if (! postLocations[post])
                    continue; 
                
                contentString = '<div id="content">'+
                      '<div id="siteNotice">'+
                      '</div><a href="/post/' +
                        postIds[post] +
                      '">'+
                      '<h1 id="firstHeading" class="firstHeading"> ' +
                        postTitles[post] +
                      '</h1></a>'+
                      '<div id="bodyContent">'+
                      '<p><a href="/post/' +
                        postIds[post] +
                      '">'+
                      'Go to post</a></p>'+
                      '</div>'+
                      '</div>';
                
                var mark = new google.maps.Marker({
                    position: new google.maps.LatLng(postLocations[post].lat, postLocations[post].lng),
                    map: map.instance,
                    title: postTitles[post],
                    data: contentString,
                    icon: image,
                    optimized: false,
                    zIndex: 9
                });
                
                infowindow = new google.maps.InfoWindow({
                    content: "holding...."
                });
                
                google.maps.event.addListener(mark, 'click', function () {
	               // where I have added .html to the marker object.
                   infowindow.setContent(this.data);
                   infowindow.open(map.instance, this);
                });
            }
            
            postLocations = [];
        }
        

      // Create and move the marker when latLng changes.
      self.autorun(function() {
        var userloc= Geolocation.latLng();
          
        if (! userloc){
            userloc = {lat: -28.001925, lng: 153.429603};
        }
          
        var image = {
            url: 'images/map-user-icon-black-ps.png',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(15, 26),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(7, 26)
        };

        // If the marker doesn't yet exist, create it.
        if (! marker) {
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(userloc.lat, userloc.lng),
            map: map.instance,
            animation: google.maps.Animation.DROP,
            icon: image,
            optimized: false,
            zIndex: 10
          });
          map.instance.setCenter(marker.getPosition());
          map.instance.setZoom(12);
        }
        // The marker already exists, so we'll just change its position.
        else {
          marker.setPosition(userloc);
        } 
      });
    });
  });
    
    
    
    
    
    
    
  Template.map.helpers({
    geolocationError: function() {
      var error = Geolocation.error();
//        Bert.alert( error.message, "map" );
      return error && error.message;
    },
    mapOptions: function() {
      var styles = 
          [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}];
        
      var latLng = Geolocation.latLng();
        
      // Initialize the map once we have the latLng.
      if (GoogleMaps.loaded() && latLng) {
        return {
          center: new google.maps.LatLng(latLng.lat, latLng.lng),
          zoom: 12,
          styles: styles
        };
      }
    },
    mapOptions2: function() {
      var styles = 
          [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}];
        
      var latLng = {lat: -28.001925, lng: 153.429603};
        
      // Initialize the map once we have the latLng.
      if (GoogleMaps.loaded() && latLng) {
        return {
          center: new google.maps.LatLng(latLng.lat, latLng.lng),
          zoom: 12,
          styles: styles
        };
      }
    }
  });
}




//------ METEOR METHODS ------//

Meteor.methods({
  addConnection: function (connection) {
    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }
      
    Connections.insert({
      createdAt: new Date(),
      owner: Meteor.userId(),
      connection_id: connection
    });
  },
  removeConnection: function (connection) {  
    var conn = Connections.findOne( { $and: [ 
        {owner: Meteor.userId() }, 
        {connection_id: connection }
    ]});
      
    Connections.remove(conn._id);
  },
    
  addPost: function (text, location) {
    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }

    Posts.insert({
      text: text,
      geo: location,
      createdAt: new Date(),
      owner: Meteor.userId(),
      username: Meteor.user().username
    });
  },
    
  deletePost: function (postId) {
    var post = Posts.findOne(postId);
    if (post.private && post.owner !== Meteor.userId()) {
      // If the task is private, make sure only the owner can delete it
      throw new Meteor.Error("not-authorized");
    }
      
    Posts.remove(postId);
  },
    
  setPostPrivate: function (postId, setToPrivate) {
    var post = Posts.findOne(postId);
 
    // Make sure only the task owner can make a task private
    if (post.owner !== Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }
 
    Posts.update(postId, { $set: { private: setToPrivate } });
  },
  
  likePost: function (postId) {
   if (! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }
      
    Likes.insert({
      post: postId,
      createdAt: new Date(),
      owner: Meteor.userId(),
    });
  },
    
  unlikePost: function (postId) {
    if (! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }
      
    var like = Likes.findOne({post: postId});
      
    Likes.remove(like._id);
  },
    
  commentPost: function (text, postId) {
    if (! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }
      
    Comments.insert({
      text: text,
      post: postId,
      createdAt: new Date(),
      owner: Meteor.userId(),
    });
  },

  updateBio: function (bioText) {
    var user =  Meteor.users.findOne(Meteor.userId());
    var profile =  Profiles.findOne({owner: user._id});
      
    Profiles.update(profile._id, { $set: { bio: bioText.bio } });
  },
    
    
  storeUrlInUserImageDatabase: function( url ) {
    check( url, String );
    validate( url );
      
      console.log("asjgdjhagsfd");
    var user =  Meteor.users.findOne(Meteor.userId());
    var userImage =  UserImages.find({userId: user._id}).count();
      
    if (userImage <= 0) {
        console.log("fresh", userImage);
        try {
          UserImages.insert({
            url: url,
            userId: Meteor.userId(),
            added: new Date() 
          });
        } catch( exception ) {
            return exception;
        }
    }
    else{
        userImage =  UserImages.findOne({userId: user._id});
        console.log("replace", userImage);
        UserImages.update(userImage._id, { $set: { url: url, added: new Date() } });
    }
  },
  
  storeUrlInPostDatabase: function( url, location, title ) {
//    check( url, String );
//    validate( url );
      
    try {
      Posts.insert({
        title: title,
        url: url,
        geo: location,
        createdAt: new Date(),
        owner: Meteor.userId(),
        username: Meteor.user().username
      });
    } catch( exception ) {
        console.log("store post exception");
        return exception;
    }
  }
});




//------ checkUrlValidity Functions ------//

let _fileExistsInDatabase = ( url ) => {
  return UserImages.findOne( { "url": url, "userId": Meteor.userId() }, { fields: { "_id": 1 } } );
};

let _isNotAmazonUrl = ( url ) => {
  return ( url.indexOf( 's3-ap-southeast-2.amazonaws.com' ) < 0 );
};

let _validateUrl = ( url ) => {
  if ( _fileExistsInDatabase( url ) ) {
    return { valid: false, error: "Sorry, this file already exists!" };
  }

  if ( _isNotAmazonUrl( url ) ) {
    return { valid: false, error: "Sorry, this isn't a valid URL!" };
  }

  return { valid: true };
};

let validate = ( url ) => {
  let test = _validateUrl( url );

  if ( !test.valid ) {
    throw new Meteor.Error( "file-error", test.error );
  }
};

//------ uploadToAmazonS3 Functions ------//

let template;

let _getFileFromInput = ( event ) => event.target.files[0];

let _setPlaceholderText = ( string = "Click or Drag a File Here to Upload" ) => {
  template.find( ".alert span" ).innerText = string;
};

let _addUrlToUserImageDatabase = ( url ) => {
  Meteor.call( "storeUrlInUserImageDatabase", url, ( error ) => {
    if ( error ) {
      Bert.alert( error.reason, "warning" );
      _setPlaceholderText();
    } else {
      Bert.alert( "File uploaded to Amazon S3!", "success" );
      _setPlaceholderText();
    }
  });
};

let _addUrlToPostDatabase = ( url, location, title ) => {
  Meteor.call( "storeUrlInPostDatabase", url, location, title, ( error ) => {
    if ( error ) {
      Bert.alert( error.reason, "warning" );
      //_setPlaceholderText();
    } else {
      Bert.alert( "File uploaded to Amazon S3!", "success" );
      //_setPlaceholderText();
    }
  });
};

let _uploadImageToAmazon = ( file, type, location, title ) => {
  const uploader = new Slingshot.Upload( "uploadToAmazonS3" );
    
  uploader.send( file, ( error, url ) => {
    if ( error ) {
      Bert.alert( error.message, "warning" );
                console.log(error);

      //_setPlaceholderText();
    } else {
        if (type == "userImage")
          _addUrlToUserImageDatabase( url );
        else
          _addUrlToPostDatabase( url, location, title );
    }
  });
};

let _uploadVidToAmazon = ( file, type, location, title ) => {
  var uploader = new Slingshot.Upload( "uploadVidToAmazonS3" );
    
  uploader.send( file, ( error, url ) => {
   if ( error ) {
     Bert.alert( error.message, "warning" );
     //_setPlaceholderText();
   } else {
     _addUrlToPostDatabase( url, location, title );
   }
 });
};

let upload = ( options ) => {
  template = options.template;
  let file = options.file;//_getFileFromInput( options.event );
  let type = options.type;
  let location = options.location;
  let title = options.title;

    console.log("upld file : ", file);
  //_setPlaceholderText( `Uploading ${file.name}...` );
//  _uploadFileToAmazon( file );
    
  if (type == "image" || type == "userImage"){
    _uploadImageToAmazon( file, type, location, title );
  }else{
    _uploadVidToAmazon( file, type, location, title );
  }
};





//------ MY FUNCTIONS ------//

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function toRad(value) {
    // Converts numeric degrees to radians
    return value * Math.PI / 180;
}

function getPostLocations(post)
{
    postLocations.push(post.geo);
    postTitles.push(post.title);
    postIds.push(post._id);
}

//function getConnectionId(conn)
//{
//    connectionIds.push(conn.connection_id);
//}

function getUserIds(connection)
{
    connectionOwners.push(connection.connection_id);
}

function blah2(connection)
{
    console.log("blha2 : ", connection.username);
}

function calcDistance(postloc, userloc)
{
    lat1 = userloc.lat;
    lon1 = userloc.lng;
    
    lat2 = postloc.lat;
    lon2 = postloc.lng;
    
    var φ1 = toRad(lat1), φ2 = toRad(lat2), Δλ = toRad(lon2-lon1), R = 6371000; // gives d in metres
    var d = Math.acos( Math.sin(φ1)*Math.sin(φ2) + Math.cos(φ1)*Math.cos(φ2) * Math.cos(Δλ) ) * R;
    
    return d;
}

function isImg(name)
{
    const formats = [ 'jpg', 'jpeg', 'png', 'gif' ];
    var res = name.split(".");
    
    var found = $.inArray(res[1], formats) > -1;
    return found;
}

function isVid(name)
{
    const formats = [ 'mp4', 'mov', 'webm', 'quicktime', '3gp' ];
    var res = name.split(".");

    var found = $.inArray(res[1], formats) > -1;
    return found;
}

function isImg2(data)
{
    var patt = /^data:([^\/]+\/[^;]+)?(;charset=([^;]+))?(;base64)?,/i, 
      matches = data.match(patt);
    
    const formats = [ 'jpg', 'jpeg', 'png', 'gif' ];
    
    var found = false;
    
    if (matches) {
        console.log(matches[1]);
        var res = matches[1].split("/");
        found = $.inArray(res[1], formats) > -1;
    }
    return found;
}

function isVid2(data)
{
    var patt = /^data:([^\/]+\/[^;]+)?(;charset=([^;]+))?(;base64)?,/i, 
      matches = data.match(patt);
    
    const formats = [ 'mp4', 'mov', 'webm', 'quicktime', '3gp' ];
   
    var found = false;
    
    if (matches) {
        console.log(matches[1]);
        var res = matches[1].split("/");
        found = $.inArray(res[1], formats) > -1;
    }
    
    return found;
}

function addfilenameType(name, data)
{
    var patt = /^data:([^\/]+\/[^;]+)?(;charset=([^;]+))?(;base64)?,/i, 
      matches = data.match(patt);
    
    const formats = [ 'jpg', 'jpeg', 'png', 'gif', 'mp4', 'mov', 'webm', 'quicktime', '3gp' ];
    
    console.log(matches[1]);
    var res = matches[1].split("/");
    
    var found = $.inArray(res[1], formats) > -1;
    
    if (!found){
        return false;
    }
    
//    var found2 = $.inArray(res[1], formats);
//    console.log("found2... ",found2);
    var newname = name + "." + res[1];
    return newname;
}




function success(pos) {
  var crd = pos.coords;

  console.log('Your current position is:');
  console.log('Latitude : ' + crd.latitude);
  console.log('Longitude: ' + crd.longitude);
  console.log('More or less ' + crd.accuracy + ' meters.');
};

function error(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
};


function createFilename() {
  return Math.random().toString(36).substr(2, 9);
}

function dataUriToFile(dataUri, fileName) {
    console.log("datauri : ", dataUri);
//    var data = dataUri.toString();
  // https://en.wikipedia.org/wiki/Data_URI_scheme
  // create a pattern to match the data uri
  var patt = /^data:([^\/]+\/[^;]+)?(;charset=([^;]+))?(;base64)?,/i, 
      matches = dataUri.match(patt);
    
  if (matches == null){
    throw new Error("data: uri did not match scheme")
  }
  var 
    prefix = matches[0],
    contentType = matches[1],
    // var charset = matches[3]; -- not used.
    isBase64 = matches[4] != null,
    // remove the prefix
    encodedBytes = dataUri.slice(prefix.length),
    // decode the bytes
    decodedBytes = isBase64 ? atob(encodedBytes) : encodedBytes,
    // return the file object
    props = {};
  if (contentType) {
    props.type = contentType;
  }
  return new File([decodedBytes], fileName, props);
}